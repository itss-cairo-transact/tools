* @ValidationCode : MjotMjEwMjE2NTI4NTpDcDEyNTI6MTY0Mzg4Njc4NDQ2MTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 03 Feb 2022 13:13:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
$PACKAGE TRANS.DIC
SUBROUTINE ITSS.UPLOAD.EB.DICTIONARY
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------
    $USING EB.SystemTables
    $USING EB.DataAccess
    $USING EB.TransactionControl

  * DEBUG

    FN.EB.DICTIONARY = "F.EB.DICTIONARY"
    F.EB.DICTIONARY = ''
    EB.DataAccess.Opf(FN.EB.DICTIONARY,F.EB.DICTIONARY)

    UPLOAD.PATH.VAR = ''
    UPLOAD.PATH = "C:\Temenos\T24\bnk\UD\Upload"
    EB.DataAccess.Opf(UPLOAD.PATH,UPLOAD.PATH.VAR)
    UPLOAD.FL.1 = "DICTIONARY.EXTRACTION4.txt"



    UPLOAD.FL = UPLOAD.FL.1
    EB.DataAccess.FRead(UPLOAD.PATH,UPLOAD.FL,R.REC.DATA,UPLOAD.PATH.VAR,PATH.ERR)

    CONVERT @FM TO @VM IN R.REC.DATA
	
    NO.REC = ''



    NO.REC = DCOUNT(R.REC.DATA,@VM)



    FOR I = 1 TO NO.REC


        CONVERT @VM TO @FM IN R.REC.DATA<I>

        DICTIONARY.ID   = FIELD(R.REC.DATA<I>, '|',1)
        TEXT            = FIELD(R.REC.DATA<I>, '|',2)
        SHORT.DESC      = FIELD(R.REC.DATA<I>, '|',3)
        Description     = FIELD(R.REC.DATA<I>, '|',4)
        PROMPT.TEXT     = FIELD(R.REC.DATA<I>, '|',5)
        ENQ.HEADER      = FIELD(R.REC.DATA<I>, '|',6)
        ENQ.SEL.TEXT    = FIELD(R.REC.DATA<I>, '|',7)
        TXT.OPERATION   = FIELD(R.REC.DATA<I>, '|',8)
        ENQ.TOOL.TEXT   = FIELD(R.REC.DATA<I>, '|',9)
        TOOL.TIP        = FIELD(R.REC.DATA<I>, '|',10)


                
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicDescription>   =  Description
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicShortDesc>     =  SHORT.DESC
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicText>          =  TEXT
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicPromptText>    =  PROMPT.TEXT
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicEnqHeader>     =  ENQ.HEADER
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicEnqSelText>    =  ENQ.SEL.TEXT
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicTxtOperation>  =  TXT.OPERATION
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicEnqToolText>   =  ENQ.TOOL.TEXT
        R.DICTIONARY<EB.SystemTables.EbDictionary.DicToolTip>       =  TOOL.TIP
                
                    CHANGE ',' TO '?' IN DICTIONARY.ID

        OFS.SOURCE.ID.1 = 'ITSS.AR.UPLOAD.DICT'
        APP.NAME.1 = 'EB.DICTIONARY'
        OFSFUNCTION.1 = 'I'
        PROCESS.1 = 'PROCESS'
        OFSVERSION.1 = 'EB.DICTIONARY,'
        NO.OF.AUTH.1 = '0'
        GTS.MODE.1 = ''
        OFSRECORD.1 = ''
        Y.RESP.OFS.1=''
        Y.OUTPUT.1=''
		
        TRANSACTION.ID.1 = DICTIONARY.ID

        CALL OFS.BUILD.RECORD(APP.NAME.1,OFSFUNCTION.1,PROCESS.1,OFSVERSION.1,GTS.MODE.1,NO.OF.AUTH.1,TRANSACTION.ID.1,R.DICTIONARY,OFSRECORD.1)
        OFS.MSG = ''
        OFS.SRC = ''
        OFS.MSG = OFSRECORD.1
        OFS.SRC = OFS.SOURCE.ID.1

        CALL OFS.CALL.BULK.MANAGER(OFS.SRC,OFS.MSG,Y.RESP.OFS,Y.OUTPUT)



        TXN.REF = FIELD(Y.RESP.OFS, '/',3)[1,1]


        IF TXN.REF EQ '1' THEN

        Y.DATA.AUTH = TRANSACTION.ID.1
           UPLOAD.SUCCESS.VAR.1<-1> = Y.DATA.AUTH
       FL.NM.AUTH = 'UPLOADSUCCESS':'.txt'
         EB.DataAccess.FWrite(UPLOAD.PATH,FL.NM.AUTH,UPLOAD.SUCCESS.VAR.1)
       *CALL F.WRITE (UPLOAD.PATH,FL.NM.AUTH,UPLOAD.PATH.VAR)

           EB.TransactionControl.JournalUpdate("")
       *CALL JOURNAL.UPDATE("")
        END ELSE

           Y.DATA.UNAUTH = TRANSACTION.ID.1:'|':Y.RESP.OFS
            UPLOAD.PATH.VAR.1<-1> = Y.DATA.UNAUTH
           FL.NM.UNAUTH = 'UPLOADFAILED':'.txt'
            EB.DataAccess.FWrite(UPLOAD.PATH,FL.NM.UNAUTH,UPLOAD.PATH.VAR.1)
            EB.TransactionControl.JournalUpdate("")
       *CALL F.WRITE (UPLOAD.PATH,FL.NM.AUTH,UPLOAD.PATH.VAR)
       *CALL JOURNAL.UPDATE("")

        END


    NEXT I


RETURN
END

