* @ValidationCode : MjozMjA5ODkzMDY6Q3AxMjUyOjE2MzM4Nzk2NzQ1OTk6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 10 Oct 2021 17:27:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
$PACKAGE EB.DICTIONARY
SUBROUTINE ITSS.EB.DICTIONARY.EXTRACT
*****************************************************************************************************************************************************************
* Author  : Abdelrahman Said
* Date    : 9/10/2021
* Comment : This Service Working for Extracting Data from EB.DICTIONARY.
*****************************************************************************************************************************************************************
    $USING EB.SystemTables
    $USING EB.DataAccess
*****************************************************************************************************************************************************************
   
    GOSUB INTIAL
    GOSUB DO.PROCESS
RETURN
*****************************************************************************************************************************************************************
INTIAL:
*******
    I = ''
    Y = ''
    Y = 1
    I = 1
    Z = ''
    Z = 5000
    X= ''
    X= 1
    FN.DICTIONARY = 'F.EB.DICTIONARY'
    F.DICTIONARY  = ''
    EB.DataAccess.Opf(FN.DICTIONARY,F.DICTIONARY)
RETURN
*****************************************************************************************************************************************************************
DO.PROCESS:
***********
    FLAT.FILE.DIR    = ''
    FLAT.FILE.DIR    = 'EB.DICTIONARY.EXTRACTION.FILEZ'
    FLAT.FILE.NAME   = ''
    FLAT.FILE.NAME   = 'DICTIONARY.EXTRACTION.txt'
    EXTRACTED.FILE     = ''
    GOSUB OPEN.FILE
    GOSUB EXTRACTION
RETURN
*****************************************************************************************************************************************************************
OPEN.FILE:
**********
    OPENSEQ FLAT.FILE.DIR,FLAT.FILE.NAME TO EXTRACTED.FILE ELSE
        CREATE EXTRACTED.FILE THEN
            OPENSEQ FLAT.FILE.DIR,FLAT.FILE.NAME TO EXTRACTED.FILE ELSE
                NULL
                RETURN
            END
        END ELSE
            CALL FATAL.ERROR('Unable to open the LINE ID Log path')
            RETURN
        END
    END
RETURN
*******************************************************************************************************************************************************************
WRITE.FILE:
***********
    
    WRITESEQ  FILE.LINE APPEND TO EXTRACTED.FILE ELSE PRINT "Unalble to Write"
* WEOFSEQ EXTRACTED.FILE


RETURN
********************************************************************************************************************************************************************
EXTRACTION:
***********
    SEL.CMD     = '' ; SEL.LIST = '' ; SEL.CNT = '' ; ERR.SEL = ''
    SEL.CMD     = "SELECT ":FN.DICTIONARY
    EB.DataAccess.Readlist(SEL.CMD,SEL.LIST,'',SEL.CNT,ERR.SEL)
    
    LOOP
    WHILE I LE Z
        DICTIONARY.ID   = ''
        DICTIONARY.ID   = SEL.LIST<I>
        NO.OF.MINS      = ''
        NO.OF.MINS      = COUNT(DICTIONARY.ID,'-')
        LENGHT.OF.ID    = ''
        LENGHT.OF.ID    = LEN(DICTIONARY.ID)
        LENGHT.OF.ID.MINS.THREE.CHAR = ''
        LENGHT.OF.ID.MINS.THREE.CHAR = LENGHT.OF.ID - 3
        LENGHT.OF.ID.MINS.THREE.CHAR = LENGHT.OF.ID.MINS.THREE.CHAR +1
        LAST.THREE.CHAR = DICTIONARY.ID[LENGHT.OF.ID.MINS.THREE.CHAR,3]
        IF NO.OF.MINS GT 1 THEN
            N= 1
        END   ELSE
            IF LAST.THREE.CHAR EQ '-CN' OR LAST.THREE.CHAR EQ '-FR' OR LAST.THREE.CHAR EQ '-AR' THEN
                N = 1
            END ELSE
                EB.DataAccess.FRead(FN.DICTIONARY,DICTIONARY.ID,R.DICTIONARY,F.DICTIONARY,ERR.DICTIONARY)

                SHORT.DESC      = ''
                PROMPT.TEXT     = ''
                ENQ.HEADER      = ''
                ENQ.SEL.TEXT    = ''
                TXT.OPERATION   = ''
                ENQ.TOOL.TEXT   = ''
                TOOL.TIP        = ''
                TEXT            = ''
                Description     = ''
                
                Description     = R.DICTIONARY<EB.SystemTables.EbDictionary.DicDescription>
                SHORT.DESC      = R.DICTIONARY<EB.SystemTables.EbDictionary.DicShortDesc>
                TEXT            = R.DICTIONARY<EB.SystemTables.EbDictionary.DicText>
                PROMPT.TEXT     = R.DICTIONARY<EB.SystemTables.EbDictionary.DicPromptText>
                ENQ.HEADER      = R.DICTIONARY<EB.SystemTables.EbDictionary.DicEnqHeader>
                ENQ.SEL.TEXT    = R.DICTIONARY<EB.SystemTables.EbDictionary.DicEnqSelText>
                TXT.OPERATION   = R.DICTIONARY<EB.SystemTables.EbDictionary.DicTxtOperation>
                ENQ.TOOL.TEXT   = R.DICTIONARY<EB.SystemTables.EbDictionary.DicEnqToolText>
                TOOL.TIP        = R.DICTIONARY<EB.SystemTables.EbDictionary.DicToolTip>
                
                FILE.LINE       = ''
                FILE.LINE       = DICTIONARY.ID:' | ':TEXT:' | ':SHORT.DESC:' | ': Description:' | ':PROMPT.TEXT: ' | ':ENQ.HEADER: ' | ':ENQ.SEL.TEXT: ' | ':TXT.OPERATION: ' | ':ENQ.TOOL.TEXT: ' | ':TOOL.TIP
               
                GOSUB WRITE.FILE
            END
        END
        I++
    REPEAT
    Y = Y + 1
    IF Y EQ 20 THEN
        I = I + 1
        Z = SEL.CNT
        GOSUB DO.PROCESS
    END ELSE
        IF Z LT SEL.CNT THEN
            I = Z +1
            Z = Z + 5000
            X = X + 1
            GOSUB DO.PROCESS
        END
    END
   
RETURN
**********************************************************************************************************************************************************************
END