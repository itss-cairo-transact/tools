* @ValidationCode : MjotMTQ1MjkwODk2MzpDcDEyNTI6MTY0NTUyODA5MDI4Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 22 Feb 2022 13:08:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0

$PACKAGE ITSS.HELPTEXTMENUEXTRACTION
SUBROUTINE ITSS.HELPTEXT.MENU.EXTRACTION
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------
    $USING EB.SystemTables
    $USING EB.DataAccess
    
    
*****************************************************************************************************************************************************************
    GOSUB INTIAL
    GOSUB DO.PROCESS
    
RETURN
*****************************************************************************************************************************************************************
INTIAL:
*******

    FN.HELPTEXT.MENU = 'F.HELPTEXT.MENU'
    F.HELPTEXT.MENU  = ''
    EB.DataAccess.Opf(FN.HELPTEXT.MENU,F.HELPTEXT.MENU)
    
RETURN
*****************************************************************************************************************************************************************
DO.PROCESS:
***********
    FLAT.FILE.DIR    = ''
    FLAT.FILE.DIR    = 'C:\Temenos\TAFJ\UD\MENU'
    FLAT.FILE.NAME   = ''
    FLAT.FILE.NAME   = 'HELPTEXT.MENUEXTRACTION.txt'
    EXTRACTED.FILE     = ''
    GOSUB OPEN.FILE
    GOSUB EXTRACTION
RETURN
*****************************************************************************************************************************************************************
OPEN.FILE:
**********
    OPENSEQ FLAT.FILE.DIR,FLAT.FILE.NAME TO EXTRACTED.FILE ELSE
        CREATE EXTRACTED.FILE THEN
            OPENSEQ FLAT.FILE.DIR,FLAT.FILE.NAME TO EXTRACTED.FILE ELSE
                NULL
                RETURN
            END
        END ELSE
            CALL FATAL.ERROR('Unable to open the LINE ID Log path')

            RETURN
        END
    END
RETURN
*******************************************************************************************************************************************************************
WRITE.FILE:
***********
    
    WRITESEQ  FILE.LINE APPEND TO EXTRACTED.FILE ELSE PRINT "Unalble to Write"
* WEOFSEQ EXTRACTED.FILE

RETURN
********************************************************************************************************************************************************************
EXTRACTION:
***********
    SEL.CMD     = '' ; SEL.LIST = '' ; SEL.CNT = '' ; ERR.SEL = ''
    SEL.CMD     = "SELECT ":FN.HELPTEXT.MENU
    EB.DataAccess.Readlist(SEL.CMD,SEL.LIST,'',SEL.CNT,ERR.SEL)
    
    I = 1
    
    LOOP
*WHILE I LE 2
    WHILE I LE SEL.CNT
        
        HELPTEXT.MENU.ID   = ''
        HELPTEXT.MENU.ID   = SEL.LIST<I>

        
        EB.DataAccess.FRead(FN.HELPTEXT.MENU,HELPTEXT.MENU.ID,R.HELPTEXT.MENU,F.HELPTEXT.MENU,ERR.HELPTEXT.MENU)
      

        HELPTEXT.MENU.APP         = ''
        HELPTEXT.MENU.DESCRIPT.EN = ''
        HELPTEXT.MENU.DESCRIPT.AR = ''

        HELPTEXT.MENU.APP             = R.HELPTEXT.MENU<EB.SystemTables.HelptextMenu.MenApplication>
		
        HELPTEXT.MENU.DESCRIPT.EN.ARRAY     = R.HELPTEXT.MENU<EB.SystemTables.HelptextMenu.MenDescript>
		HELPTEXT.MENU.DESCRIPT.AR.ARRAY     = R.HELPTEXT.MENU<EB.SystemTables.HelptextMenu.MenDescript>
		
		CONVERT @VM TO @FM IN HELPTEXT.MENU.DESCRIPT.EN.ARRAY
		CONVERT @VM TO @FM IN HELPTEXT.MENU.DESCRIPT.AR.ARRAY
		CONVERT @VM TO @FM IN HELPTEXT.MENU.APP
		
		
		DESCRIPT.COUNT = DCOUNT(HELPTEXT.MENU.DESCRIPT.EN.ARRAY,@FM)
		
		*PRINT  DESCRIPT.EN.COUNT :"-":HELPTEXT.MENU.DESCRIPT.EN.ARRAY
		DESCRIPT.INDEX = 1
		
		*FILE.LINE       = ''
        *FILE.LINE       = HELPTEXT.MENU.ID:' | ':HELPTEXT.MENU.APP<1,1>:' | ':HELPTEXT.MENU.DESCRIPT.EN.ARRAY<1,1>
		*FILE.NAME = DESCRIPT.EN.COUNT:"|":HELPTEXT.MENU.DESCRIPT.EN.ARRAY
		*: " | " :HELPTEXT.MENU.DESCRIPT.AR.ARRAY<1,1>
		*GOSUB WRITE.FILE
		
		LOOP 
		WHILE DESCRIPT.INDEX LE DESCRIPT.COUNT
			FILE.LINE       = ''
			IF DESCRIPT.INDEX EQ 1 THEN
				*FILE.LINE       = HELPTEXT.MENU.ID:' | ':HELPTEXT.MENU.APP<DESCRIPT.INDEX,1>:' | ':HELPTEXT.MENU.DESCRIPT.EN.ARRAY<DESCRIPT.INDEX,1>
				FILE.LINE       = HELPTEXT.MENU.ID:' | ':HELPTEXT.MENU.DESCRIPT.EN.ARRAY<DESCRIPT.INDEX,1>
			END
			ELSE
				FILE.LINE       = ' | ' : HELPTEXT.MENU.DESCRIPT.EN.ARRAY<DESCRIPT.INDEX,1>
			END
			*: " | " : HELPTEXT.MENU.DESCRIPT.AR.ARRAY<DESCRIPT.INDEX,2>
			
			GOSUB WRITE.FILE
			DESCRIPT.INDEX++
		REPEAT

		DESCRIPT.INDEX = 1
       *HELPTEXT.MENU.DESCRIPT.AR     = R.HELPTEXT.MENU<EB.SystemTables.HelptextMenu.MenDescript ,1,2>
		
            
   *     IF HELPTEXT.MENU.DESCRIPT.AR EQ '' THEN
   *         HELPTEXT.MENU.DESCRIPT.AR = "FALSE"
   *
   *     END ELSE
   *         IF HELPTEXT.MENU.DESCRIPT.AR NE '' THEN
   *             HELPTEXT.MENU.DESCRIPT.AR = "TRUE"
   *
   *         END
   *     END
		
           
        *FILE.LINE       = ''
        *FILE.LINE       = HELPTEXT.MENU.ID:' | ':HELPTEXT.MENU.APP:' | ':HELPTEXT.MENU.DESCRIPT.EN
		*:' | ': HELPTEXT.MENU.DESCRIPT.AR
        
               
        *GOSUB WRITE.FILE
		
        I++
        
    REPEAT

RETURN
**********************************************************************************************************************************************************************
END