* @ValidationCode : MjotMTQ1OTA0Mzc2OkNwMTI1MjoxNjQ1MzU2MjQzNDc2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 20 Feb 2022 13:24:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0

$PACKAGE ERROR.EXTRACT
SUBROUTINE ITSS.EB.ERROR.EXTRACTION

    $USING EB.SystemTables
    $USING EB.DataAccess
    $USING EB.ErrorProcessing
*****************************************************************************************************************************************************************
   
    GOSUB INTIAL
    GOSUB DO.PROCESS
    
RETURN
*****************************************************************************************************************************************************************
INTIAL:
*******

    FN.EB.ERROR = 'F.EB.ERROR'
    F.EB.ERROR  = ''
    EB.DataAccess.Opf(FN.EB.ERROR,F.EB.ERROR)
    
RETURN
*****************************************************************************************************************************************************************
DO.PROCESS:
***********
    FLAT.FILE.DIR    = ''
    FLAT.FILE.DIR    = 'C:\Temenos\TAFJ\UD\ERROR'
    FLAT.FILE.NAME   = ''
    FLAT.FILE.NAME   = 'EB.ERROR.EXTRACTION.txt'
    EXTRACTED.FILE     = ''
    GOSUB OPEN.FILE
    GOSUB EXTRACTION
RETURN
*****************************************************************************************************************************************************************
OPEN.FILE:
**********
    OPENSEQ FLAT.FILE.DIR,FLAT.FILE.NAME TO EXTRACTED.FILE ELSE
        CREATE EXTRACTED.FILE THEN
            OPENSEQ FLAT.FILE.DIR,FLAT.FILE.NAME TO EXTRACTED.FILE ELSE
                NULL
                RETURN
            END
        END ELSE
            EB.ErrorProcessing.FatalError('Unable to open the LINE ID Log path')
            RETURN
        END
    END
RETURN
*******************************************************************************************************************************************************************
WRITE.FILE:
***********
    
    WRITESEQ  FILE.LINE APPEND TO EXTRACTED.FILE ELSE PRINT "Unalble to Write"
* WEOFSEQ EXTRACTED.FILE

RETURN
********************************************************************************************************************************************************************
EXTRACTION:
***********
    SEL.CMD     = '' ; SEL.LIST = '' ; SEL.CNT = '' ; ERR.SEL = ''
    SEL.CMD     = "SELECT ":FN.EB.ERROR
    EB.DataAccess.Readlist(SEL.CMD,SEL.LIST,'',SEL.CNT,ERR.SEL)
    
    I = 1
    
    LOOP
	  *WHILE I LE 2
    WHILE I LE SEL.CNT
        
        EB.ERROR.ID   = ''
        EB.ERROR.ID   = SEL.LIST<I>

        
        EB.DataAccess.FRead(FN.EB.ERROR,EB.ERROR.ID,R.EB.ERROR,F.EB.ERROR,ERR.EB.ERROR)
        

        ERROR.MSG      = ''
                
        *EB.ERROR.ID  =  R.EB.ERROR<EB.ErrorProcessing.EodError.ErrApplicationId>
         ERROR.MSG     = R.EB.ERROR<EB.ErrorProcessing.Error.ErrErrorMsg>
        
                
        FILE.LINE       = ''
        FILE.LINE       = EB.ERROR.ID:' | ':ERROR.MSG
               
        GOSUB WRITE.FILE
        I++
		
    REPEAT

RETURN
**********************************************************************************************************************************************************************
END