* @ValidationCode : MjotMTgwMDY3ODk0OTpDcDEyNTI6MTYzMzg3NTQ0MjQzMTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 10 Oct 2021 16:17:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
$PACKAGE EB.ARMENU
SUBROUTINE ITSS.MAIN.UPLOAD.ARMENU
*-----------------------------------------------------------------------------
*
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.HELPTEXT.MENU
*-----------------------------------------------------------------------------
* Modification History :
*-----------------------------------------------------------------------------

*-----------------------------------------------------------------------------
    AUTH.PATH = "C:\Temenos\TAFJ\UD\ARMENU"
*DEBUG
    CALL OPF(AUTH.PATH,AUTH.PATH.VAR)
    AUTH.FL.1 = "ITSS.HELPTEXT.MENU.UPLOAD.txt"
    FN.HELPTEXT.MENU = "F.HELPTEXT.MENU"
    F.HELPTEXT.MENU = ''
    CALL OPF(FN.HELPTEXT.MENU,F.HELPTEXT.MENU)
    
    Y.SEL.CMD = 'SELECT ':AUTH.PATH
    CALL EB.READLIST(Y.SEL.CMD,Y.SEL.LIST,'',Y.SEL.CNT,SEL.ERR)
    AUTH.FL = AUTH.FL.1
    CALL F.READ(AUTH.PATH,AUTH.FL,R.REC.DATA,AUTH.PATH.VAR,PATH.ERR)
 

    CRLF = CHARX(013):CHARX(010)
    R.REC.DATA = CHANGE(R.REC.DATA, CRLF, @FM)
        
    NO.REC = DCOUNT(R.REC.DATA,@FM)

    FOR I = 1 TO NO.REC
* DATA.FLAG = 'NO'

        DATA.REC = R.REC.DATA<I>
        TABLE.ID = FIELD(R.REC.DATA<I>, '|',1)
		IF TABLE.ID EQ 'FINANCE.OPS.REPORTS.AU' THEN
			DEBUG
		END
		PRINT "[":I:"] id : ":FIELD(R.REC.DATA<I>, '|',1)
        BEGIN CASE
            CASE TABLE.ID NE ''
*****NEW TABLE ID ******
                COUNT.X = '1'
				
                Y.NEW.ID = FIELD(R.REC.DATA<I>, '|',1)
                Y.RECORD.ID = FIELD(R.REC.DATA<I>, '|',2)
                Y.AR.ID  = FIELD(R.REC.DATA<I>, '|',3)
				
                CALL F.READ(FN.HELPTEXT.MENU,Y.NEW.ID,R.HELPTEXT.MENU,F.HELPTEXT.MENU,ERR.HELPTEXT.MENU)
				R.HELPTEXT.MENU<EB.MEN.DESCRIPT,COUNT.X,2> = Y.AR.ID
				CALL LOG.WRITE(FN.HELPTEXT.MENU,Y.NEW.ID,R.HELPTEXT.MENU,'')
*                CALL F.WRITE(FN.HELPTEXT.MENU,Y.NEW.ID,R.HELPTEXT.MENU)
                *CALL JOURNAL.UPDATE('')

            CASE TABLE.ID EQ ''
*******SAME TABLE**************
               COUNT.X = COUNT.X + '1'
               Y.RECORD.ID = FIELD(R.REC.DATA<I>, '|',2)
               Y.AR.ID  = FIELD(R.REC.DATA<I>, '|',3)
               CALL F.READ(FN.HELPTEXT.MENU,NEW.ID,R.HELPTEXT.MENU,F.HELPTEXT.MENU,ERR.HELPTEXT.MENU)
               *R.HELPTEXT.MENU<EB.MEN.DESCRIPT,COUNT.X,2> = RECORD.ID
               *R.HELPTEXT.MENU<EB.MEN.DESCRIPT,COUNT.X,3> = RECORD.ID
               *R.HELPTEXT.MENU<EB.MEN.DESCRIPT,COUNT.X,4> = RECORD.ID
               R.HELPTEXT.MENU<EB.MEN.DESCRIPT,COUNT.X,2> = Y.AR.ID
			   CALL LOG.WRITE(FN.HELPTEXT.MENU,Y.NEW.ID,R.HELPTEXT.MENU,'')
               *CALL F.WRITE(FN.HELPTEXT.MENU,Y.NEW.ID,R.HELPTEXT.MENU)
               *CALL JOURNAL.UPDATE('')
        END CASE
    NEXT I
END
